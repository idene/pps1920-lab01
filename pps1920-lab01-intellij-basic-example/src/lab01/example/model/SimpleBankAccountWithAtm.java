package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount {

    public SimpleBankAccountWithAtm(final AccountHolder holder, final double balance) {
        super(holder, balance);
    }
/*
* if (checkUser(usrID)) {
            this.balance += amount-1;
        }
*
* */
    public void depositWithAtm(int usrID, double amount) {
        super.deposit(usrID, (amount > 0) ? (amount - 1) : 0);
    }

    public void withdrawWithAtm(int usrID, double amount) {

        super.withdraw(usrID, (amount > 0) ? (amount + 1) : 0);
    }
}
